﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DragCard : MainDragItem
{

    public Transform Canvas;
    Transform Target;

    Vector3 worldPosition;

    public bool DeletStarItemEndDrag;
    public bool DeletNewItemEndDrag;

    public bool activeblocksRaycastsEndDrag;

    public bool PrespetiveCamera;


    public Vector3 addPosition;

    public GameObject Panel;

    public bool Drag;

    public bool OffDrag = false;//Отключение возможности перемещения

    private void Awake()
    {
        if (!GetComponent<CanvasGroup>()) gameObject.AddComponent<CanvasGroup>();
    }

    public override void StarDragItem()
    {
        if (Drag)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = false;
            transform.position = new Vector3(transform.position.x, transform.position.y + addPosition.x, transform.position.z);

        }
    }

    public override void DragItem()
    {
        if (Drag)
        {
            if (GetComponent<ShawdownCard>()) GetComponent<ShawdownCard>().ShowdowEnablet(true);

            if (PrespetiveCamera)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitData;

                if (Physics.Raycast(ray, out hitData, 1000))
                {
                    worldPosition = hitData.point;
                }
            }
            else
            {
                worldPosition = Input.mousePosition;
            }

            transform.position = new Vector3(worldPosition.x + addPosition.x, addPosition.y, worldPosition.z + addPosition.z);

        }
    }

    public override void EndDragItem()
    {
        if (Drag)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;


            transform.position = new Vector3(worldPosition.x, -2.8f, worldPosition.z);
 

            if (DeletStarItemEndDrag) Destroy(gameObject);
            if (Target != null)
            {
                if (DeletNewItemEndDrag) Destroy(Target.gameObject);

                if (activeblocksRaycastsEndDrag) Target.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }
  
 
            GridMap.instant.AlingItem(gameObject);
            Drag = false;
        }

    }

    public override void onItemClick()
    {
        if (Input.GetMouseButtonUp(0))
        {
            ManagerSignal.Push<Signals.CloseSelectCard>(this);


            if (GetComponent<SityController>() != null)
            {
                Panel.SetActive(true);
                GetComponent<SityController>().startPanel();
            }
            Drag = false;
 

        }
    }

    public override void onItemClickDown()
    {
        if (OffDrag == false)
        {
            if (Input.GetMouseButtonDown(0))
            {        
                Drag = true;
            }
        }
    }


}
