﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainShawdownCard : MonoBehaviour
{
    public GameObject ShawdownGo;
    [HideInInspector]
    public GameObject NewShawdow;
    [HideInInspector]
    public Transform Pool;//Пул теней

    private void Awake()
    {
       // Pool = ManagerPrefab.get.PoolShadow.transform;
    }
    //public Canvas canvas;

    public float y;
    public float scale;

    // Start is called before the first frame update
    void Start()
    {
        NewShawdow = Instantiate(ShawdownGo, transform.position, transform.rotation, Pool);
        //NewShawdow.transform.SetParent(canvas.transform);

    }

    // Update is called once per frame
    void Update()
    {
        NewShawdow.transform.position = new Vector3(transform.position.x, y, transform.position.z);
        NewShawdow.transform.localScale = new Vector3(scale, scale, scale);

    }

    public void LayerRander(int layer)
    {
        NewShawdow.GetComponent<Canvas>().sortingOrder = layer;

    }

    public void ShowdowEnablet(bool on)
    {
        if (NewShawdow != null)
            NewShawdow.SetActive(on);
        //NewShawdow.active = on;
    }
    //Удаление тени
    public void ShadowDel()
    {
        Destroy(NewShawdow);
    }
    private void OnDestroy()
    {
        ShadowDel();
    }
}
