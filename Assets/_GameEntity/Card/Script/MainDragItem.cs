﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public abstract class MainDragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler
{
    public void OnBeginDrag(PointerEventData eventData)
    {
        StarDragItem();
    }

    public void OnDrag(PointerEventData eventData)
    {
        DragItem();
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        EndDragItem();
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        onItemClick();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        onItemClickDown();
    }

    public abstract void StarDragItem();
    public abstract void DragItem();
    public abstract void EndDragItem();

    public virtual void onItemClick() {}
    public virtual void onItemClickDown() { }

}
