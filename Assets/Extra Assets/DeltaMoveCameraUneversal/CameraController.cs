﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraOrintation
{
    Top,Front
}

public class CameraController : MonoBehaviour
{

    public CameraOrintation Orintation;

    public float SpeedMove;
    public Camera cam;
    public Camera camPrespective;
    public float zoom;
    public float stepZoom;

    Vector3 MouseStart;
    Vector3 MouseMove;



    Vector3 current_position = Vector3.zero;

    public Transform target;


    bool drag;

    public Vector3 upRot;
    public Vector3 downRot;

    // Use this for initialization
    void Start()
    {

    }

    float x;
    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(2))
        {
            MouseStart = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zoom);
            MouseStart = cam.ScreenToWorldPoint(MouseStart);

            if (Orintation == CameraOrintation.Front) MouseStart.z = transform.position.z;
            else MouseMove.y = transform.position.y;


            drag = true;

        }
        if (Input.GetMouseButtonUp(2))
        {
            Debug.Log("GetMouseButtonUp");
      

        }


        else if (Input.GetMouseButton(2))
        {

            MouseMove = new Vector3(Input.mousePosition.x, Input.mousePosition.y, zoom);
            MouseMove = cam.ScreenToWorldPoint(MouseMove);
            if(Orintation == CameraOrintation.Front) MouseMove.z = transform.position.z;
            else MouseMove.y = transform.position.y;
            target.position = MouseMove;
        }

        
        if (Vector3.Distance(current_position, transform.position) > 0.1  && drag)
            transform.position = transform.position - (target.position - MouseStart) * Time.deltaTime * SpeedMove;


        var nextZoom = Input.GetAxis("Mouse ScrollWheel");


        if (nextZoom > 0)
        {
            
          if(Orintation == CameraOrintation.Front)  cam.orthographicSize -= nextZoom * stepZoom;
            else
            {
               if (-20 < camPrespective.transform.localPosition.z)
                {
                    Vector3 camPos = camPrespective.transform.position;
                    Vector3 camEndPos = new Vector3(camPos.x, camPos.y + stepZoom, camPos.z);
                    // camPrespective.transform.position = new Vector3(camPos.x, camPos.y + stepZoom, camPos.z);

                    camPrespective.transform.position = Vector3.Lerp(camPos, camEndPos, Time.deltaTime * 0.1f);
/*
                    Quaternion rightRot = Quaternion.Euler(upRot);
                    camPrespective.transform.rotation = Quaternion.Lerp(camPrespective.transform.rotation, rightRot, Time.deltaTime * 1f);
*/


                }


            }
            

           
        }

       if (nextZoom < 0)
        {
            
          if (Orintation == CameraOrintation.Front) cam.orthographicSize -= nextZoom * stepZoom;
          else
            {
                if (-8 > camPrespective.transform.localPosition.z)
                {
                    Vector3 camPos = camPrespective.transform.position;
                    Vector3 camEndPos = new Vector3(camPos.x, camPos.y - stepZoom, camPos.z);
                    //camPrespective.transform.position = new Vector3(camPos.x, camPos.y - stepZoom, camPos.z);

                    camPrespective.transform.position = Vector3.Lerp(camPos, camEndPos, Time.deltaTime * 0.1f);
                    /*
                                        Quaternion rightRot = Quaternion.Euler(downRot);
                                        camPrespective.transform.rotation = Quaternion.Lerp(camPrespective.transform.rotation, rightRot, Time.deltaTime * 1f);
                    */
                }
            }
            
        }

    }
}
