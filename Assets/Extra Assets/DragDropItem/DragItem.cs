﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class DragItem : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IPointerDownHandler
{
    public Transform Canvas;
    Transform Target;

    Vector3 worldPosition;

    public bool createNewItem;

    public bool DeletStarItemEndDrag;
    public bool DeletNewItemEndDrag;

    public bool activeblocksRaycastsEndDrag;

    public bool PrespetiveCamera;


    public Vector3 addPosition;

    public GameObject Panel;

    public bool Drag;

    private void Awake()
    {
        if(!GetComponent<CanvasGroup>()) gameObject.AddComponent<CanvasGroup>();
/*
        ManagerSignal.Subscribe<Signals.SelectTible>(signal =>
        {
            if (Panel != null)
            {
                //Panel.SetActive(false);
                GetComponent<SityController>().closePanel();
            }

        });

        ManagerSignal.Subscribe<Signals.CloseSelectCard>(sinal => 
        {
            //if (Panel != null) Panel.SetActive(false);
            GetComponent<SityController>().closePanel();
        });
*/
    }

    public void OnBeginDrag(PointerEventData eventData)
    {
        if (Drag)
        {
            //transform.SetParent(Canvas);
           

            GetComponent<CanvasGroup>().blocksRaycasts = false;
            if (createNewItem)
            {
                Target = Instantiate(transform);
                Target.transform.SetParent(Canvas);
            }

            transform.position = new Vector3(transform.position.x, transform.position.y + addPosition.x, transform.position.z);

            GetComponent<ShawdownCard>().y = -2.7f;
            GetComponent<ShawdownCard>().scale = 1.05f;
            GetComponent<ShawdownCard>().LayerRander(1);
            transform.SetSiblingIndex(1);
            GetComponent<SityController>().closePanel();
        }

        if (transform.parent != Canvas) Drag = false;
      
    }

    public void OnDrag(PointerEventData eventData)
    {
        if (Drag)
        {
            GetComponent<ShawdownCard>().ShowdowEnablet(true);

            if (PrespetiveCamera)
            {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hitData;

                if (Physics.Raycast(ray, out hitData, 1000))
                {
                    worldPosition = hitData.point;
                }
            }
            else
            {
                worldPosition = Input.mousePosition;
            }

            if (createNewItem) Target.position = worldPosition + addPosition;


            else transform.position = worldPosition + addPosition;

            //Panel.SetActive(false);
        
            transform.localScale = new Vector3(1, 1, 1);

        }
    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (Drag)
        {
            GetComponent<CanvasGroup>().blocksRaycasts = true;


            transform.position = new Vector3(worldPosition.x, -2.8f, worldPosition.z);

            if (DeletStarItemEndDrag) Destroy(gameObject);
            if (Target != null)
            {
                if (DeletNewItemEndDrag) Destroy(Target.gameObject);

                if (activeblocksRaycastsEndDrag) Target.GetComponent<CanvasGroup>().blocksRaycasts = true;
            }

            GetComponent<ShawdownCard>().y = -2.9f;
            GetComponent<ShawdownCard>().scale = 1;
            GetComponent<ShawdownCard>().LayerRander(-1);
            transform.SetSiblingIndex(1);

        }

        GridMap.instant.AlingItem(gameObject);
        Drag = false;

    }

    public void OnPointerClick(PointerEventData eventData)
    {
        if (Input.GetMouseButtonUp(0))
        {
            ManagerSignal.Push<Signals.CloseSelectCard>(this);

            //Panel.SetActive(true);
            GetComponent<SityController>().startPanel();
            Drag = false;
            transform.SetSiblingIndex(transform.parent.childCount);

        }
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (Input.GetMouseButtonDown(0))
        {
            Drag = true;
        }
    }

}
