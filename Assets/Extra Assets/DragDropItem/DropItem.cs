﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;


public class DropItem : MonoBehaviour, IDropHandler, IPointerEnterHandler, IPointerExitHandler
{

    public void OnDrop(PointerEventData eventData)
    {
        DragItem drag = eventData.pointerDrag.GetComponent<DragItem>();
        if (drag != null)
        {
            eventData.pointerDrag.GetComponent<CanvasGroup>().blocksRaycasts = true;
            eventData.pointerDrag.transform.SetParent(transform);
        }
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            eventData.pointerDrag.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
            eventData.pointerDrag.GetComponent<DragItem>().Drag = false;
            eventData.pointerDrag.transform.position = new Vector3(transform.position.x, transform.position.y + 0.02f, transform.position.z) ;
            eventData.pointerDrag.GetComponent<ShawdownCard>().ShowdowEnablet(false);
        }
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        if (eventData.pointerDrag != null)
        {
            DragItem dragItem = eventData.pointerDrag.GetComponent<DragItem>();
            dragItem.Drag = true;
            if (dragItem.transform.parent != dragItem.Canvas) dragItem.transform.SetParent(dragItem.Canvas);
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
