using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class CardItem  
{
    public GameObject Card;
    public Vector2 position_cell;
}
