﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum StateSity
{
    Start,
    Find,
    End
}

public class SityController : MonoBehaviour
{
    public GameObject panelStart;
    public GameObject panelWork;
    public GameObject panelEnd;

    public StateSity stateSity;


    GameObject actionPanel;


    // Start is called before the first frame update
    void Start()
    {
        ManagerSignal.Subscribe<Signals.SelectTible>(signal => closePanel());
        ManagerSignal.Subscribe<Signals.CloseSelectCard>(sinal => closePanel());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void startPanel()
    {
        switch (stateSity)
        {
            case StateSity.Start:
                panelStart.SetActive(true);
                actionPanel = panelStart;
                break;
            case StateSity.Find:
                panelWork.SetActive(true);
                actionPanel = panelWork;
                break;
            case StateSity.End:
                panelEnd.SetActive(true);
                actionPanel = panelEnd;
                break;
            default:
                break;
        }
    }

    public void closePanel()
    {
        if (actionPanel != null) actionPanel.SetActive(false);
    }


    public void Find()
    {
        stateSity = StateSity.Find;
    }


}
