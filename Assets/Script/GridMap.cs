﻿using Sirenix.OdinInspector;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GridMap : MonoBehaviour
{

    public GameObject CanvarCard;
    public GameObject HelperGrid;
    public Transform Card_pool;
    public Transform Grid_pool;

    // public KashGlobal Kash;

    public static GridMap instant;

    [Header("Размер ячеек")]
    public Vector2 CellSize;
    [Header("Размер карты")]
    public Vector2 CellMap;
    [Header("Высота карты")]
    public float heightMap;

    public bool CreateGridHelper;
 
    public List<GameObject> CardArrey = new List<GameObject>();

    [Header("Стар сетки")]
    public Vector2 StartMap;



    private void Awake()
    {
        instant = this;
    }

    private void Start()
    {
        BuildHelperGrid();
        AligItemAll();
    }
    [Button("BuildGridHelper")]
    public void BuildHelperGrid()
    {

        if (CreateGridHelper)
        {
            ClearHelperGrid();

            for (int x = 0; x < CellMap.x; x++)
            {
                for (int y = 0; y < CellMap.y; y++)
                {
                    GameObject go = Instantiate(HelperGrid, new Vector3(x / CellSize.x * CellMap.x + StartMap.x, heightMap, y / CellSize.y * CellMap.y + StartMap.y), CanvarCard.transform.rotation);

                    go.transform.SetParent(Grid_pool);
                }
            }
        }
    }
    [Button("ClearGridHelper")]
    public void ClearHelperGrid()
    {
 
        while(Grid_pool.transform.childCount > 0)
        {
            DestroyImmediate(Grid_pool.transform.GetChild(0).gameObject);
        }
      

    }
    [Button("AligCardAll")]

    public void AligItemAll()
    {



        for (int i = 0; i < Card_pool.transform.childCount; i++)
        {

            Vector3 pos = Card_pool.transform.GetChild(i).transform.position;

            float Aling_x = Mathf.Round(((pos.x - StartMap.x) * CellSize.x / CellMap.x));
            float Aling_y =   Mathf.Round(((pos.z - StartMap.y) * CellSize.y / CellMap.y) );

            Card_pool.transform.GetChild(i).transform.position = new Vector3((Aling_x / CellSize.x * CellMap.x) + StartMap.x, pos.y, Aling_y / CellSize.y * CellMap.y + StartMap.y); 
        }


    }

    public void AlingItem(GameObject item)
    {
 
            Vector3 pos = item.transform.position;

            float Aling_x = Mathf.Round(((pos.x - StartMap.x) * CellSize.x / CellMap.x));
            float Aling_y = Mathf.Round(((pos.z - StartMap.y) * CellSize.y / CellMap.y));

            item.transform.transform.position = new Vector3((Aling_x / CellSize.x * CellMap.x) + StartMap.x, pos.y, Aling_y / CellSize.y * CellMap.y + StartMap.y);
 
    }

    public Vector2 GetPositionCell(Vector2 cell)
    {

        return default;
    }
 
}
 