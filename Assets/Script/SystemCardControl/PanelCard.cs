﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[System.Serializable]
public class ItemPanel
{
    public GameObject Panel;
    public string Name;

}

public class PanelCard : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler
{
    //public List<ItemPanel> Panel;

    public GameObject PanelGO;

    public Button button;

    public void OnPointerClick(PointerEventData eventData)
    {
         print("OnPointerClick");

        PanelGO.SetActive(true);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        print("OnPointerEnter");
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        print("OnPointerExit");
    }




    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

    }
}
