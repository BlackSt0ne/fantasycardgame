﻿using System.Collections;
using System.Collections.Generic;
//using System.Numerics;
using UnityEngine;

public class ShawdownCard : MonoBehaviour
{

    public GameObject ShawdownGo;

    GameObject NewShawdow;

    public Canvas canvas;

    public float y;
    public float scale;

    // Start is called before the first frame update
    void Start()
    {
        NewShawdow = Instantiate(ShawdownGo, transform.position, transform.rotation);
        NewShawdow.transform.SetParent(canvas.transform);

    }

    // Update is called once per frame
    void Update()
    {
        NewShawdow.transform.position = new Vector3(transform.position.x, y, transform.position.z);
        NewShawdow.transform.localScale = new Vector3(scale, scale, scale);

    }

    public void LayerRander(int layer)
    {
        NewShawdow.GetComponent<Canvas>().sortingOrder = layer;

    }

    public void ShowdowEnablet(bool on)
    {
        NewShawdow.active = on;
    }
}
