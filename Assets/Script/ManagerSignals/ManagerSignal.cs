﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ManagerSignal : MonoBehaviour
{

    public bool log;
    static public ManagerSignal managerSignal;

    private void Awake()
    {
        managerSignal = this;
    }

    public static Global<T> Subscribe<T>(Action<T> A)
    {
        Global<T> G = new Global<T>();
        G.A = A;

        List<Action<T>> cachedSignals;
        var key = typeof(T).GetHashCode();

        if (BaseDataSignals<T>.SubscribeSignals.TryGetValue(key, out cachedSignals))
        {

            cachedSignals.Add(A);
            return G;
        }

        BaseDataSignals<T>.SubscribeSignals.Add(key, new List<Action<T>> { A });

        return G;
    }



    public static void Push<T>(ref T data, object o)
    {

        if (managerSignal.log) Debug.Log(data + " " + o);

        List<Action<T>> cachedSignals;

        if (!BaseDataSignals<T>.SubscribeSignals.TryGetValue(typeof(T).GetHashCode(), out cachedSignals)) return;

        var len = cachedSignals.Count;

        for (var i = 0; i < len; i++)
        {
            cachedSignals[i](data);
        }

        Clear<T>();

    }

    public static ManagerSignal Push<T>(object o) where T : new()
    {
        //var data = new T();

        if (managerSignal.BlackList<T>(o.GetHashCode())) return managerSignal;

        var data = managerSignal.BackSignal<T>(o.GetHashCode());

        List<Action<T>> cachedSignals;

        if (!BaseDataSignals<T>.SubscribeSignals.TryGetValue(typeof(T).GetHashCode(), out cachedSignals)) return managerSignal;

        var len = cachedSignals.Count;

        for (var i = 0; i < len; i++)
        {
            cachedSignals[i](data);
        }

        Clear<T>();

        return managerSignal;
    }

    public static ManagerSignal Push<T>(Action<T> A, object o) where T : new()
    {

        // var data = new T();

        if (managerSignal.BlackList<T>(o.GetHashCode())) return managerSignal;

        var data = managerSignal.BackSignal<T>(o.GetHashCode());

        List<Action<T>> cachedSignals;

        if (!BaseDataSignals<T>.SubscribeSignals.TryGetValue(typeof(T).GetHashCode(), out cachedSignals)) return managerSignal;

        var len = cachedSignals.Count;

        for (var i = 0; i < len; i++)
        {
            A(data);
            cachedSignals[i](data);
        }

        Clear<T>();

        return managerSignal;
    }


    public static void Dispose<T>(Action<T> A)
    {

        List<Action<T>> cachedSignals;
        var key = typeof(T).GetHashCode();

        if (BaseDataSignals<T>.ClearSignals.TryGetValue(typeof(T).GetHashCode(), out cachedSignals))
        {

            //  Debug.Log("unsubscribe " + cachedSignals.Count);
            cachedSignals.Add(A);
            return;
        }

        BaseDataSignals<T>.ClearSignals.Add(key, new List<Action<T>> { A });
    }


    static void Clear<T>()
    {

        List<Action<T>> ClearSignals;
        List<Action<T>> cachedSignals;

        var key = typeof(T).GetHashCode();

        if (!BaseDataSignals<T>.ClearSignals.TryGetValue(typeof(T).GetHashCode(), out ClearSignals)) return;

        if (BaseDataSignals<T>.SubscribeSignals.TryGetValue(key, out cachedSignals))
        {
            var len = ClearSignals.Count;

            for (int i = 0; i < len; i++)
            {
                cachedSignals.Remove(ClearSignals[i]);
            }


            return;
        }


    }

    T BackSignal<T>(int key) where T : new()
    {
        List<T> PushSignals;
        T NewPushSignal;
        if (BaseDataSignals<T>.PushSignals.TryGetValue(key, out PushSignals))
        {
            var size = PushSignals.Count;
            for (int i = 0; i < size; i++)
            {
                if (typeof(T) == PushSignals[i].GetType()) return PushSignals[i];
            }

            NewPushSignal = new T();
            PushSignals.Add(NewPushSignal);
            return NewPushSignal;
        }

        NewPushSignal = new T();
        BaseDataSignals<T>.PushSignals.Add(key, new List<T> { NewPushSignal });

        return NewPushSignal;
    }
    Type NewSignal;
    int NewKey;
    bool BlackList<T>(int key)
    {
        List<LimitBlackList> BlackList;
        NewSignal = typeof(T);
        NewKey = key;
        if (DataType.LimitBlackList.TryGetValue(key, out BlackList))
        {
            var size = BlackList.Count;
            for (int i = 0; i < size; i++)
            {
                if (typeof(T) == BlackList[i].typeSignal)
                {
                    if(BlackList[i].size != 0) return false;
                    else return true;
                }
            }
            return false;
        }

        return false;
    }

    public void Limit(int count)
    {

        List<LimitBlackList> LimitBlackList;

        if (DataType.LimitBlackList.TryGetValue(NewKey, out LimitBlackList))
        {

            var size = LimitBlackList.Count;

            for (int i = 0; i < size; i++)
            {
                if (NewSignal == LimitBlackList[i].typeSignal)
                {
                   if(LimitBlackList[i].size > 0) LimitBlackList[i].size--;
                }
            }
            
            return;
        }

        LimitBlackList Limit = new LimitBlackList(count-1, NewSignal);
        DataType.LimitBlackList.Add(NewKey, new List<LimitBlackList> { Limit });

    }

}

public class DataType
{
    //public static readonly Dictionary<int, List<Type>> BlackListSignal = new Dictionary<int, List<Type>>();
    public static readonly Dictionary<int, List<LimitBlackList>> LimitBlackList = new Dictionary<int, List<LimitBlackList>>();
}

public class BaseDataSignals<T>
{
    public static bool log;





    public static readonly Dictionary<int, List<T>> PushSignals = new Dictionary<int, List<T>>();
    public static readonly Dictionary<int, List<Action<T>>> SubscribeSignals = new Dictionary<int, List<Action<T>>>();

    public static readonly Dictionary<int, List<Action<T>>> ClearSignals = new Dictionary<int, List<Action<T>>>();

    public static List<Action<T>> cachedSignals;


}

public class Global<T> : IDisposable
{

    public Action<T> A;

    public void Dispose()
    {
        List<Action<T>> cachedSignals;

        var key = typeof(T).GetHashCode();

        if (BaseDataSignals<T>.SubscribeSignals.TryGetValue(key, out cachedSignals))
        {

            cachedSignals.Remove(A);

            return;
        }
    }

    public void unsubscribe()
    {
        List<Action<T>> cachedSignals;
        var key = typeof(T).GetHashCode();

        if (BaseDataSignals<T>.ClearSignals.TryGetValue(typeof(T).GetHashCode(), out cachedSignals))
        {
            cachedSignals.Add(A);
            return;
        }

        BaseDataSignals<T>.ClearSignals.Add(key, new List<Action<T>> { A });
    }

}

public class LimitBlackList
{
   public int size = 0;
   public Type typeSignal;

    public LimitBlackList(int size, Type typeSignal)
    {
         this.size = size;
        this.typeSignal = typeSignal;
    }


}







