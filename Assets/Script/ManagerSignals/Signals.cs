﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Signals 
{
    public class SelectTible
    {
        public string info = "SelectTible";
    }

    public class SelectCard
    {
        public string info = "SelectCard";
    }

    public class CloseSelectCard
    {
        public string info = "CloseSelectCard";
    }

    public class DragCart
    {
        public string info = "DragCart";
    }

    public class Test
    {
        public string info = "DragCart";

    }

}
