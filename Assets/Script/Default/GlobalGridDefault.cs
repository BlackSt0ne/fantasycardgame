﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class GlobalGridDefault : MonoBehaviour
{
    /*
    /// <summary>
    /// Глобальный кэш
    /// </summary>
    public KashGlobal Kash;
   
    public static GlobalGridDefault instant;
    [Header("Размер ячеек по осям")]
    public Vector2 CellSize;
    [Header("Список объектов, дочек, которых нужно выравнивать")]
    public List<GameObject> AllObjects = new List<GameObject>();
    // [HideInInspector]
    public List<GameObject> DialogWindowCells = new List<GameObject>();
    [Header("Список объектов, для выравнивания без сортировки")]
    public List<GameObject> ObjList = new List<GameObject>();
    [Header("Пул теней")]
    public Transform PoolShadow;
    // [HideInInspector]
    // public List<GameObject> CartInPool = new List<GameObject>();
    [Header("Начало отсчета")]
    public Vector2 StartMap;
    [Header("Конец отсчета")]
    public Vector2 EndMap;
    int g = 0;

    //Добавляем объекты в массив
    public void AddInMass(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            DialogWindowCells.Add(parent.GetChild(i).gameObject);
        }
    }
    //Удаляем объекты из массива
    public void DelIsMass(Transform parent)
    {
        for (int i = 0; i < parent.childCount; i++)
        {
            GameObject f = parent.GetChild(i).gameObject;
            for (int i1 = DialogWindowCells.Count - 1; i1 > -1; i1--)
            {
                if (DialogWindowCells[i1] == f)
                {
                    DialogWindowCells.RemoveAt(i1);
                    break;
                }
            }
        }
    }
    private void Awake()
    {
        instant = this;
    }
    //Выравнивание по сетке по сетке
    public void PlacementOnGrid()
    {
        for (int i = 0; i < AllObjects.Count; i++)
        {
            PlacementOnGrindIsObj(AllObjects[i]);
        }
        for (int i = 0; i < ObjList.Count; i++)
        {
            PlacementOnGridObj(ObjList[i]);
        }
    }
    //Метод растановки по сетке
    private void PlacementOnGrindIsObj(GameObject AllObj)
    {
        for (int i = 0; i < AllObj.transform.childCount; i++)
        {
            PlacementOnGridObj(AllObj.transform.GetChild(i).gameObject);
        }
    }
    //Установка карты в позицию на столе 
    //Неверно, так как не выставляется в дефаулт положенгие, а каждый раз расчитывает занова, нужно еременную, что бы понимать
    //В начальное подожение или в конечное
    void DefaultPosition(GameObject obj, Vector3 pos, bool def)//Последний параметр для определения дефаулт позишон
    {
        if (def == true)
        {
            pos = obj.GetComponent<CartInfo>().oldPos;
            // Vector3 caellPos = PosCartFind(pos);
        }

        Vector3 a = PosCartFind(pos);
        //Проверяем новую позицию на актуальность
        a = CheckingForRelevance(a);
        g = 0;
        obj.transform.position = a;
        if (obj.transform.parent.name == "CartPool")
        {
            obj.transform.parent.parent.GetComponent<OldPanelControl>().DelList(obj);//Удаляем карту из массивов
        }
        if (obj.transform.parent != Kash.Canvas.transform) obj.transform.SetParent(Kash.Canvas.transform);
    }
    //Ищем ячейку по координатам
    private Vector3 PosCartFind(Vector3 pos)
    {
        Vector3 b = new Vector3((pos.x - StartMap.x), pos.y, (pos.z - StartMap.y));
        Vector3 a = new Vector3((Mathf.Round(b.x / CellSize.x) * CellSize.x) + StartMap.x, pos.y, ((Mathf.Round(b.z / CellSize.y) * CellSize.y)) + StartMap.y);
        return a;
    }
    //Ищем карту квеста
    private GameObject FindQuestCart(Vector3 pos, GameObject cart)
    {
        Vector3 newPos = PosCartFind(pos);//Новая позиция
        List<GameObject> cartAll = CreateMassGameObjects(AllObjects);//Массив  всех карт на столе
        GameObject checkedCart = FindGoObjList(newPos, cartAll);//Поиск карты по координатам
        if (checkedCart != null)
        {
            CartInfo cartInfo = checkedCart.GetComponent<CartInfo>();
            if (cartInfo.CartInform.cartType == CartType.quest)
            {
                SityController sityControler = checkedCart.GetComponent<SityController>();
                OldPanelControl oldPanelControl = sityControler.panelStart.GetComponent<OldPanelControl>();
                if (oldPanelControl.DoINeedCart(cart.GetComponent<CartInfo>().CartInform) == true)
                {
                    oldPanelControl.AddCart(cart, oldPanelControl.FindKoofAllPole());//Добавляем карту
                }
                else
                {
                    checkedCart = null;
                }
            }
            else
            {
                checkedCart = null;
            }
        }
        return checkedCart;
    }
    //Расстановка конкретной карты
    public void PlacementOnGridObj(GameObject obj)
    {
        Vector3 pos = obj.transform.position;//Кэш позиции
                                             //Вычисляем новую позицию
                                             //Сначала проверяем нет ли квестовой карты там
        GameObject QuestCart = FindQuestCart(pos, obj);

        if (QuestCart == null)
        {
            GameObject cell = FindKartInCellDialogWindow(pos, obj.GetComponent<CartInfo>().CartInform);//Прверяем, на наличае желания поместит карту в ячейку
            if (cell == null)
            {
                DefaultPosition(obj, pos, false);
            }
            else
            {
                Transform slot = cell.transform.parent.parent;//Берем ссылку на панель
                if (slot.GetComponent<OldPanelControl>().DoINeedCart(obj.GetComponent<CartInfo>().CartInform) == true)
                {
                    OldPanelControl OPC = slot.GetComponent<OldPanelControl>();
                    OPC.AddCart(obj, cell);//Добавляем карту
                }
                else
                {
                    DefaultPosition(obj, pos, true);
                }
            }
        }
    }
    //Проверка, а не вячейку диалогового окна мы хотим поместить эту карту
    GameObject FindKartInCellDialogWindow(Vector3 pos, CartObjInfo cart)
    {
        if (DialogWindowCells.Count != 0)
        {
            for (int i = 0; i < DialogWindowCells.Count; i++)
            {
                GameObject cell = DialogWindowCells[i];
                Vector3 posX = cell.transform.position;
                if (cell.activeSelf == true)
                {
                    if (pos.x < posX.x + CellSize.x / 2 && pos.x > posX.x - CellSize.x / 2 && pos.z < posX.z + CellSize.y / 2 && pos.z > posX.z - CellSize.y / 2)
                    {
                        return cell;
                    }
                }
            }
        }
        return null;
    }
    //Проверяем новую позицию на актуальность
    Vector3 CheckingForRelevance(Vector3 pos)
    {
        //Во-первых, проверим, не выходит ли карта за границу
        if (pos.x > EndMap.x) pos = new Vector3(pos.x - CellSize.x, pos.y, pos.z);
        if (pos.x < StartMap.x) pos = new Vector3(pos.x + CellSize.x, pos.y, pos.z);
        if (pos.z > EndMap.y) pos = new Vector3(pos.x, pos.y, pos.z + CellSize.y);
        if (pos.z < StartMap.y) pos = new Vector3(pos.x, pos.y, pos.z - CellSize.y);
        //Во-вторых, проверяем, есть ли по этм координатам уже карты или нет
        GameObject findGO = null;
        List<GameObject> OldGO = new List<GameObject>();
        bool a = false;
        while (a != true)
        {
            findGO = FindGoObjList(pos, ObjList);
            if (findGO == null)
            {
                List<GameObject> l = CreateMassGameObjects(AllObjects);
                findGO = FindGoObjList(pos, l);
            }
            if (findGO != null)
            {
                OldGO.Add(findGO);
                Vector2 posNew = NewPos(pos);
                pos = new Vector3(pos.x + posNew.x, pos.y, pos.z + posNew.y);
            }
            if (findGO == null)
            {
                a = true;
            }
        }
        return pos;
    }
    //Создаем массив из дочерних элементов объекта, взятых из массива всех объектов
    private List<GameObject> CreateMassGameObjects(List<GameObject> allCart)
    {
        List<GameObject> l = new List<GameObject>();
        for (int i2 = 0; i2 < allCart.Count; i2++)
        {
            for (int i3 = 0; i3 < allCart[i2].transform.childCount; i3++)
            {
                l.Add(allCart[i2].transform.GetChild(i3).gameObject);
            }
        }
        return l;
    }
    //Поиск определенного ГО в массиве
    GameObject FindGoObjList(Vector3 pos, List<GameObject> h)
    {
        const float a = 0.1f;
        Vector3 pos1 = new Vector3(pos.x + a, pos.y, pos.z + a);
        pos = new Vector3(pos.x - a, pos.y, pos.z - a);
        for (int i = 0; i < h.Count; i++)
        {
            GameObject nGameObj = h[i];
            Vector3 nPos = nGameObj.transform.position;
            if (pos1.x > nPos.x && pos.x < nPos.x && pos1.z > nPos.z && pos.z < nPos.z)
            {
                return nGameObj;
            }
        }
        return null;
    }
    Vector2 NewPos(Vector3 pos)
    {
        bool a = false;
        while (a != true)
        {
            switch (g)
            {
                case 0:
                    if (pos.x + CellSize.x > EndMap.x) g = 1;
                    else return new Vector2(CellSize.x, 0);
                    break;
                case 1:
                    if (pos.x - CellSize.x < StartMap.x) g = 2;
                    else return new Vector2(CellSize.x * -1, 0);
                    break;
                case 2:
                    if (pos.z + CellSize.y > EndMap.y) g = 3;
                    else return new Vector2(0, CellSize.y);
                    break;
                case 3:
                    return new Vector2(0, CellSize.y * -1);
            }
        }
        return new Vector2(0, 0);
    }
    */
}
[System.Serializable]
public class KartInCell
{
    public GameObject Kart;//Карта
    public GameObject Cell;//Ячейка из диалогового окна
}

/// <summary>
/// Весь кэш
/// </summary>
[System.Serializable]
public class KashGlobal
{
    /// <summary>
    /// Тпймер на экране
    /// </summary>
    public GameObject TimerWindow;
    /// <summary>
    /// Кэш канваса
    /// </summary>
    public GameObject Canvas;
    /// <summary>
    /// Кэш канваса с неактивными картами
    /// </summary>
    public GameObject CanvasNoActive;//
}